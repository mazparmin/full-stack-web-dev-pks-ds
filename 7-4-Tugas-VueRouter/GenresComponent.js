export const GenresComponent = {
    data () {
        return {
            genres: [
                {
                    id: 1,
                    title: 'Action'
                },
                {
                    id: 2,
                    title: 'Western'
                },
                {
                    id: 3,
                    title: 'Family'
                },
                
            ]
        }
    },
    template:   `
                <div>
                    <h1>Daftar Genre</h1>
                    <ul>
                        <li v-for="genre of genres">
                            <router-link :to="'/genre/'+genre.id"> 
                                {{ genre.title }} 
                            </router-link>
                        </li>
                    </ul>
                </div>
                ` 
}