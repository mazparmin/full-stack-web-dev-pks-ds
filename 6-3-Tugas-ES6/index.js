//Soal No. 1
const luas = (p,l) =>  p*l
      keliling  = (p,l) => 2*(p+l)

console.log(luas(5,4))
console.log(keliling(5,4))

// Soal No. 2
const newFunction = (firstName, lastName) =>  console.log(`${firstName} ${lastName}`)   
newFunction("William", "Imoh")

//Soal No. 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
  const {firstName, lastName, address, hobby } = newObject
  console.log(firstName, lastName, address, hobby)

  //Soal No.4
  const west = ["Will", "Chris", "Sam", "Holly"]
  const east = ["Gill", "Brian", "Noel", "Maggie"]
  const combined = [...west, ...east]
  console.log(combined)

  //Soal No.5
  const planet = "earth" 
  const view = "glass" 
  //var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
  var before = `Lorem   ${view}  dolor sit amet, consectetur adipiscing elit,${planet}` 
  console.log(before)