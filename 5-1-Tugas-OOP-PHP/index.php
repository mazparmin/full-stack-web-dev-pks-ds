<?php

abstract  class Hewan {
        public $nama;
        public $darah = 50;
        public $jumlahkaki;
        public $keahlian;

        public function __construct( $darah, $jumlahkaki, $keahlian)
        {            
            $this->darah = $darah;
            $this->jumlahkaki = $jumlahkaki;
            $this->keahlian = $keahlian;
        }
        public function atraksi($nama, $keahlian){
            return $nama . " Sedang " . $keahlian;
          
         }
        
    }
    

abstract   class Fight extends Hewan{
        public $attackPower; 
        public $defencePower;
        
        public function serang($nama, $lawan){
            return $nama . " menyerang ". $lawan ;
        }
        public function diserang($nama){
            return $nama . " Diserang";
        }

    }

class Elang extends Fight {

    
}

class Harimau extends Fight {

    
}

//Ketika Elang diinstansiasi, maka jumlahKaki bernilai 2, dan keahlian bernilai "terbang tinggi", attackPower = 10 , deffencePower = 5 ;
$elang1 = new Elang(0, 2,"terbang tinggi");
$elang1 ->attackPower = 10 ; 
$elang1 ->deffencePower = 5 ;


//Ketika Harimau diintansiasi, maka jumlahKaki bernilai 4, dan keahlian bernilai "lari cepat" , attackPower = 7 , deffencePower = 8 ;
$harimau1 = new Harimau(0, 4, "lari cepat"); 
$elang1 ->attackPower = 7 ; 
$elang1 ->deffencePower = 8 ;



?>