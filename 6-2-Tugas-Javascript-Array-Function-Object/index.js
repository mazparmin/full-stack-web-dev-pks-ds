//Soal No.1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort()
daftarHewan.forEach(function(item){
    console.log(item)
 })

 // Soal No.2

function introduce(bio ={name:"", age : 0 , address : "" , hobby : ""}) 
{

    return 'Nama Saya ' + bio.name + ', Umur Saya ' + bio.age + ', Alamat Saya di ' 
    + bio.address+ ', dan saya punya hobby yaitu ' + bio.hobby
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

//soal no.3
function hitung_huruf_vokal(kata){
    var vokal=["a","i","u","e","o"]
    var jumlah=0

    for (let i = 0; i < kata.length; i++) {   
        ketemu = false    
        for (let j = 0; j < vokal.length; j++) {
            if (kata[i].toUpperCase() == vokal[j].toUpperCase() ) 
                 ketemu = true
            }
        if (ketemu) jumlah += 1
        }

    return jumlah
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

//soal no 4
function hitung(bil) {
    var temp =-2

    for ( var i=0;  i<bil; i++) {
        temp = temp + 2
    }
    return temp
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8