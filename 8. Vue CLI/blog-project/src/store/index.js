import Vue from 'vue'
import VueX from 'vuex'
//import counter from './counter';
import alert from './alert';
import dialog from './dialog';


Vue.use(VueX)


export default new VueX.Store({
  modules : {
    //counter, 
    alert, dialog
  }
  
})